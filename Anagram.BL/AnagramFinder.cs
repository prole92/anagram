﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anagram.BL
{
    public class AnagramFinder
    {

        public AnagramFinder(List<string> words)
        {
            if (words == null)
                throw new ArgumentNullException("words");
            if (words.Count() == 0)
                throw new ArgumentException("Words list must not be empty");
            this.words = words;
        }
        public List<List<string>> GetAnagrams()
        {
            PopulateAnagrams();
            return Anagrams.Values.ToList();
        }
        /// <summary>
        /// Gets the first list of longest anagrams. if there are more anagrams with the same length this method returns only the first
        /// </summary>
        public List<string> GetLongestAnagram()
        {
            PopulateAnagrams();
            int max = Anagrams.Keys.Max(x => x.Length);
            return Anagrams.FirstOrDefault(x => x.Key.Length == max).Value;
        }

        /// <summary>
        /// Gets all the lists of longest anagrams. If there are more anagrams with the same length this method returns all of them
        /// </summary>
        public List<List<string>> GetAllLongestAnagrams()
        {
            PopulateAnagrams();
            List<List<string>> allLongestWords = new List<List<string>>();
            int max = Anagrams.Keys.Max(x => x.Length);
            foreach(var keyValuePair in Anagrams)
            {
                if (keyValuePair.Key.Length == max)
                    allLongestWords.Add(keyValuePair.Value);
            }
            return allLongestWords;
        }

        public List<string> GetLongestSet()
        {
            PopulateAnagrams();
            int max = Anagrams.Values.Max(x => x.Count);
            return Anagrams.FirstOrDefault(x => x.Value.Count() == max).Value;
        }
        public List<List<string>> GetAllLongestSets()
        {
            PopulateAnagrams();
            int max = Anagrams.Values.Max(x => x.Count);
            return Anagrams.Values.Where(x => x.Count() == max).ToList();
        }

        private Dictionary<string, List<string>> FindAnagrams(List<string> words)
        {
            var anagramsDictionary = new Dictionary<string, List<string>>();

            foreach (string word in words)
            {
                if (word == null) continue;

                var orderedKey = String.Concat(word.OrderBy(x => x));
                if (anagramsDictionary.Keys.Contains(orderedKey))
                    continue;
                anagramsDictionary[orderedKey] = words.Where(x => word.IsAnagram(x)).ToList();
            }

            return anagramsDictionary;
        }


        private readonly List<string> words;
        private Dictionary<string, List<string>> Anagrams;
        private void PopulateAnagrams()
        {
            if (Anagrams == null)
            {
                Anagrams = FindAnagrams(this.words);
            }
        }
    }
}
