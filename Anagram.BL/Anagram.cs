﻿using System.Linq;

namespace Anagram
{
    public static class Anagram
    {
        public static bool IsAnagram(this string s1, string s2)
        {
            if (string.IsNullOrEmpty(s1) || string.IsNullOrEmpty(s2))
                return false;

            if (s1.Length != s2.Length)
                return false;

            s1 = string.Concat(s1.OrderBy(x => x));
            s2 = string.Concat(s2.OrderBy(x => x));
            return s1.Equals(s2);
        }
    }
}
