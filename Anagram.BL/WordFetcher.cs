﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Anagram.BL
{
    public class WordFetcher
    {
        public WordFetcher(IWordRepository wordRepository)
        {
            WordRepository = wordRepository;
        }

        public List<string> GetWords()
        {
            var words = WordRepository.GetWords().ToList();
            if (words.Any())
                return words;
            else
                throw new WordsNotFoundInRepository("No words were found in the repository");
        }

        IWordRepository WordRepository;
    }

    public class WordsNotFoundInRepository : Exception
    {
        public WordsNotFoundInRepository()
        {
        }

        public WordsNotFoundInRepository(string message)
            : base(message)
        {
        }

        public WordsNotFoundInRepository(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
