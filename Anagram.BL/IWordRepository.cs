﻿using System.Collections.Generic;

namespace Anagram.BL
{
    public interface IWordRepository
    {
        IEnumerable<string> GetWords();
    }
}
