﻿using System.Collections.Generic;
using System.IO;

namespace Anagram.BL
{
    public class WordFileRepository : IWordRepository
    {
        public WordFileRepository(string fileName)
        {
            FileName = fileName;
        }

        public readonly string FileName;

        public IEnumerable<string> GetWords()
        {
            return File.ReadAllLines(FileName);
        }
    }
}
