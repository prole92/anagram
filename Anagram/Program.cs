﻿using Anagram.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Anagram
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Count() == 0)
                {
                    Console.WriteLine("Please provide file name containing words as the first argument");
                    return;
                }

                string fileName = args[0];

                try
                {
                    ProcessWords(fileName);
                }
                catch (FileNotFoundException e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
                catch (WordsNotFoundInRepository e)
                {
                    Console.WriteLine(e.Message);
                    return;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private static void ProcessWords(string fileName)
        {
            //TODO: Create a separate module for this method so that it can unit tested
            IWordRepository wordRepository = new WordFileRepository(fileName);

            var wordFetcher = new WordFetcher(wordRepository);
            List<string> words = wordFetcher.GetWords();

            var anagramFinder = new AnagramFinder(words);

            var anagrams = anagramFinder.GetAnagrams();
            PrintAnagrams(anagrams, "Anagrams: ");

            var longestSets = anagramFinder.GetAllLongestSets();
            PrintAnagrams(longestSets, "Anagram sets containing the most words: ");

            var longestAnagrams = anagramFinder.GetAllLongestAnagrams();
            PrintAnagrams(longestAnagrams, "Anagram sets containing the longest words: ");
        }

        private static void PrintAnagrams(List<List<string>> anagrams, string anagramDescription="")
        {
            //TODO: Create a separate module for this method so that it can unit tested
            Console.WriteLine(anagramDescription);
            foreach (var anagramList in anagrams)
            {
                foreach (string anagram in anagramList)
                    Console.Write(anagram + " ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}
