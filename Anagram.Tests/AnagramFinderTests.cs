﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Anagram.BL.Tests
{
    [TestClass()]
    public class AnagramFinderTests
    {
        [TestMethod()]
        public void FindAnagramsTestValid()
        {
            var words = new List<string>()
            {
                "enlist", "inlets", "listen", "silent",
                "boaster", "boaters", "borates",
                "fresher", "refresh",
                "sinks", "skins",
                "knits", "stink",
                "rots", "sort",
                "cat", "tac"
            };
            var enlistExpected = new List<string>(){ "enlist", "inlets", "listen", "silent" };
            var boasterExpected = new List<string>(){ "boaster", "boaters", "borates" };
            var fresherExpected = new List<string>(){ "fresher", "refresh" };
            var sinksExpected = new List<string>(){ "sinks", "skins" };
            var knitsExpected = new List<string>(){ "knits", "stink" };
            var rotsExpected = new List<string>(){  "rots", "sort" };
            var catExpected = new List<string>(){ "cat", "tac" };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetAnagrams();

            Assert.AreEqual(7, actual.Count());
            CollectionAssert.AreEqual(enlistExpected, actual[0]);
            CollectionAssert.AreEqual(boasterExpected, actual[1]);
            CollectionAssert.AreEqual(fresherExpected, actual[2]);
            CollectionAssert.AreEqual(sinksExpected, actual[3]);
            CollectionAssert.AreEqual(knitsExpected, actual[4]);
            CollectionAssert.AreEqual(rotsExpected, actual[5]);
            CollectionAssert.AreEqual(catExpected, actual[6]);
        }

        [TestMethod()]
        public void FindAnagramsTestValidNoAnagrams()
        {
            var words = new List<string>()
            {
                "enlist",
                "boaster",
                "fresher",
                "sinks",
                "knits",
                "rots",
                "cat",
            };
            var enlistExpected = new List<string>() { "enlist", };
            var boasterExpected = new List<string>() { "boaster", };
            var fresherExpected = new List<string>() { "fresher", };
            var sinksExpected = new List<string>() { "sinks", };
            var knitsExpected = new List<string>() { "knits", };
            var rotsExpected = new List<string>() { "rots", };
            var catExpected = new List<string>() { "cat", };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetAnagrams();

            Assert.AreEqual(7, actual.Count());
            CollectionAssert.AreEqual(enlistExpected, actual[0]);
            CollectionAssert.AreEqual(boasterExpected, actual[1]);
            CollectionAssert.AreEqual(fresherExpected, actual[2]);
            CollectionAssert.AreEqual(sinksExpected, actual[3]);
            CollectionAssert.AreEqual(knitsExpected, actual[4]);
            CollectionAssert.AreEqual(rotsExpected, actual[5]);
            CollectionAssert.AreEqual(catExpected, actual[6]);
        }

        [TestMethod()]
        public void FindAnagramsTestSomeOfTheWordsAreSpaces()
        {
            var words = new List<string>()
            {
                "enlist",
                "boaster",
                "fresher",
                "sinks",
                "  ",
                "  ",
                "  ",
            };
            var enlistExpected = new List<string>() { "enlist", };
            var boasterExpected = new List<string>() { "boaster", };
            var fresherExpected = new List<string>() { "fresher", };
            var sinksExpected = new List<string>() { "sinks", };
            var emptyExpected = new List<string>() { "  ","  ", "  " };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetAnagrams();

            Assert.AreEqual(5, actual.Count());
            CollectionAssert.AreEqual(enlistExpected, actual[0]);
            CollectionAssert.AreEqual(boasterExpected, actual[1]);
            CollectionAssert.AreEqual(fresherExpected, actual[2]);
            CollectionAssert.AreEqual(sinksExpected, actual[3]);
            CollectionAssert.AreEqual(emptyExpected, actual[4]);
        }

        [TestMethod()]
        public void FindAnagramsTestSomeOfTheWordsAreNull()
        {
            var words = new List<string>()
            {
                "enlist",
                "boaster",
                "fresher",
                "sinks",
                "  ",
                null,
                null,
            };
            var enlistExpected = new List<string>() { "enlist", };
            var boasterExpected = new List<string>() { "boaster", };
            var fresherExpected = new List<string>() { "fresher", };
            var sinksExpected = new List<string>() { "sinks", };
            var knitsExpected = new List<string>() { "  "};

            var anagramFinder = new AnagramFinder(words);

            var actual = anagramFinder.GetAnagrams();

            CollectionAssert.AreEqual(enlistExpected, actual[0]);
            CollectionAssert.AreEqual(boasterExpected, actual[1]);
            CollectionAssert.AreEqual(fresherExpected, actual[2]);
            CollectionAssert.AreEqual(sinksExpected, actual[3]);
            CollectionAssert.AreEqual(knitsExpected, actual[4]);
        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentNullException), "Words argument should not be null.")]
        public void FindAnagramsTestWordsListIsNull()
        {
            List<string> words = null;

            var anagramFinder = new AnagramFinder(words);

        }

        [TestMethod()]
        [ExpectedException(typeof(ArgumentException), "Words list should not be empty.")]
        public void FindAnagramsTestWordsListIsEmpty()
        {
            List<string> words = new List<string>();

            var anagramFinder = new AnagramFinder(words);
        }
        [TestMethod()]
        public void GetLongestSetTest()
        {
            var words = new List<string>()
            {
                "enlist", "inlets", "listen", "silent",
                "boaster", "boaters", "borates", "broates",
                "fresher", "refresh",
                "sinks", "skins",
                "knits", "stink",
                "rots", "sort",
                "cat", "tac"
            };
            var expected = new List<string>() { "enlist", "inlets", "listen", "silent" };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetLongestSet();
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void GetAllLongestSetsTest()
        {
            var words = new List<string>()
            {
                "enlist", "inlets", "listen", "silent",
                "boaster", "boaters", "borates", "broates",
                "fresher", "refresh",
                "sinks", "skins",
                "knits", "stink",
                "rots", "sort",
                "cat", "tac"
            };
            var expected = new List<List<string>>()
            {
                new List<string>(){ "enlist", "inlets", "listen", "silent" },
                new List<string>(){ "boaster", "boaters", "borates", "broates" },
            };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetAllLongestSets();

            for(int i =0;i<expected.Count(); i++)
            {
                CollectionAssert.AreEqual(expected[i], actual[i]);
            }
        }

        [TestMethod()]
        public void GetLongestWordTest()
        {
            var words = new List<string>()
            {
                "enlist", "inlets", "listen", "silent",
                "boaster", "boaters", "borates", "broates",
                "fresher", "refresh",
                "sinks", "skins",
                "knits", "stink",
                "rots", "sort",
                "cat", "tac"
            };
            var expected = new List<string>() { "boaster", "boaters", "borates", "broates" };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetLongestAnagram();

            CollectionAssert.AreEqual(expected, actual);
        }
        [TestMethod()]
        public void GetAllLongestWordsTest()
        {
            var words = new List<string>()
            {
                "enlistt", "inltets", "litsten", "sitlent",
                "boaster", "boaters", "borates", "broates",
                "fresher", "refresh",
                "sinks", "skins",
                "knits", "stink",
                "rots", "sort",
                "cat", "tac"
            };
            var expected = new List<List<string>>()
            {
                new List<string>(){ "enlistt", "inltets", "litsten", "sitlent" },
                new List<string>() { "boaster", "boaters", "borates", "broates" },
                new List<string>() { "fresher", "refresh" },

            };

            var anagramFinder = new AnagramFinder(words);
            var actual = anagramFinder.GetAllLongestAnagrams();

            Assert.AreEqual(expected.Count(), actual.Count());
            for (int i = 0; i < expected.Count(); i++)
            {
                CollectionAssert.AreEqual(expected[i], actual[i]);
            }
        }
    }
}