﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Anagram.Tests
{
    [TestClass]
    public class AnagramTests
    {
        [TestMethod]
        public void IsAnagramTestValid()
        {
            string s1 = "abcdefghijklmnopqrstuvwxyz";
            string s2 = "abdcefghijklmnopqrstuvwxyz";

            bool expected = true;

            bool actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsAnagramTestNegative()
        {
            string s1 = "abcdefghijklmnopqrstuvwxyzabc";
            string s2 = "abdcefghijklmnopqrstuvwxyz";

            bool expected = false;

            bool actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsAnagramTestArgumentsAreNull()
        {
            string s1 = "abcdefghijklmnopqrstuvwxyzabc";
            string s2 = null;

            bool expected = false;

            bool actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsAnagramTestArgumentsAreWhiteSpace()
        {
            string s1 = "abc";
            string s2 = "";

            bool expected = false;

            bool actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);

            s1 = "";
            actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsAnagramTestLengthIsDifferent()
        {
            string s1 = "aabc";
            string s2 = "abc";

            bool expected = false;

            bool actual = s1.IsAnagram(s2);
            Assert.AreEqual(expected, actual);
        }
    }
}
